# converter  FXBO Test

## Установка

git clone git@gitlab.com:Imperia1986/converter.git
cd converter

переименовать example.env в .env и прописать подключение к базе данных

composer install

выполнить миграцию

php bin/console doctrine:migrations:migrate

проверить есть ли две консольные команды в спейсе "app"
php bin/console list

если все ок - далее команды для задачи

## Дано

2 источника котировок:

https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml - котировки ecb для разных валют

https://api.coindesk.com/v1/bpi/historical/close.json - котировки для биткоинов в долларах

## Нужно

Написать конвертер валют на symfony:


1) Написать команду для импорта данных из этих источников (Предусмотреть возможность легкого добавления новых источников).


команда для импорта xml, где первый параметр ссылка, а второй указывается основная валюта


php bin/console app:import https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml EUR


команда для импорта json, где первый параметр ссылка, а второй указывается основная валюта, третий - из какой конвертируется


php bin/console app:import https://api.coindesk.com/v1/bpi/historical/close.json USD BTC

(берётся только последнее число)


логика такая, что новые данные заменяют старые, таким образом, таблица всегда актуальна и нет дублирующих записей.



2) Написать консольную команду для конвертации валют. Если нет прямой конвертации, то пытаться конвертировать через другую валюту. Команда принимает на вход 3 параметра: сумму, которую нужно конвертировать и 2 валюты from и to. Команда может выдавать результат конвертации в произвольном виде, например “1 BTC = 32747.77 EUR”.


php bin/console app:converter 2 BTC USD

php bin/console app:converter 2 USD BTC

php bin/console app:converter 1 DKK BTC

php bin/console app:converter 1 BTC DKK

php bin/console app:converter 1 EUR BTC

php bin/console app:converter 1 BTC EUR


php bin/console app:converter 1 EUR JPY

php bin/console app:converter 1 JPY EUR

php bin/console app:converter 1 USD JPY

php bin/console app:converter 1 JPY USD


php bin/console app:converter 1 DKK JPY

php bin/console app:converter 1 JPY DKK

php bin/console app:converter 1 EUR USD

php bin/console app:converter 1 USD EUR
