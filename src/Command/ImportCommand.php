<?php

// src/Command/ImportCommand.php
namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\CssSelector\CssSelector;
use App\Entity\Quote;
use Doctrine\Persistence\ManagerRegistry;

// the name of the command is what users type after "php bin/console"
#[AsCommand(name: 'app:import')]

class ImportCommand extends Command
{
    protected static $defaultName = 'app:import';

    protected static $defaultDescription = 'Import from json';
    protected ManagerRegistry $doctrine;

    public function __construct(bool $requirePassword = false, ManagerRegistry $doctrine)
    {
        // best practices recommend to call the parent constructor first and
        // then set your own properties. That wouldn't work in this case
        // because configure() needs the properties set in this constructor
        $this->requirePassword = $requirePassword;
        $this->doctrine = $doctrine;


        parent::__construct();
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $link = $input->getArgument('link');
        $currencyTo = $input->getArgument('currencyTo');

        $pathParts = pathinfo($link);

        $entityManager = $this->doctrine->getManager();

        switch ($pathParts['extension']) {
            case 'xml':
                $crawler = new Crawler();
                $crawler->addXmlContent(
                    file_get_contents($link)
                );

                foreach ($crawler->filterXPath('//*[@currency]') as $domElement) {


                    $quote = $entityManager->getRepository(Quote::class)->findOneBy(['currencyFrom' => $currencyTo, 'currencyTo' => $domElement->getAttribute('currency')]);

                    if ($quote === null) {
                        $quote = new Quote();
                    }

                    $quote->setCurrencyFrom($currencyTo);
                    $quote->setCurrencyTo($domElement->getAttribute('currency'));
                    $quote->setRate($domElement->getAttribute('rate'));

                    // tell Doctrine you want to (eventually) save the Quote (no queries yet)
                    $entityManager->persist($quote);

                    // actually executes the queries (i.e. the INSERT query)
                    $entityManager->flush();

                }
                $output->writeln('Import xml is good!');

                break;
            case 'json':
                $bpiList = file_get_contents($link);

                $currencyFrom = $input->getArgument('currencyFrom');

                $json = json_decode($bpiList, true);

                $quote = $entityManager->getRepository(Quote::class)->findOneBy(['currencyFrom' => $currencyFrom, 'currencyTo' => $currencyTo]);

                if ($quote === null) {
                    $quote = new Quote();
                }

                $quote->setCurrencyFrom($currencyFrom);
                $quote->setCurrencyTo($currencyTo);
                $quote->setRate(end($json['bpi']));

                // tell Doctrine you want to (eventually) save the Quote (no queries yet)
                $entityManager->persist($quote);

                // actually executes the queries (i.e. the INSERT query)
                $entityManager->flush();

                $output->writeln('Import json is good!');
                
                break;
        }
        
        
        // ... put here the code to create the user

        // this method must return an integer number with the "exit status code"
        // of the command. You can also use these constants to make code more readable

        // return this if there was no problem running the command
        // (it's equivalent to returning int(0))
        return Command::SUCCESS;

        // or return this if some error happened during the execution
        // (it's equivalent to returning int(1))
        // return Command::FAILURE;

        // or return this to indicate incorrect command usage; e.g. invalid options
        // or missing arguments (it's equivalent to returning int(2))
        // return Command::INVALID
    }

    protected function configure(): void
    {
        $this
            // the command help shown when running the command with the "--help" option
            ->setHelp('This command import quotes.')
            ->addArgument('link', InputArgument::REQUIRED, 'link')
            ->addArgument('currencyTo', InputArgument::REQUIRED, 'currencyTo')
            ->addArgument('currencyFrom', InputArgument::OPTIONAL, 'currencyFrom')
        ;
    }
}