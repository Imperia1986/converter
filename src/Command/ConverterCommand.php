<?php

// src/Command/ConverterCommand.php
namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use App\Entity\Quote;
use Doctrine\Persistence\ManagerRegistry;

// the name of the command is what users type after "php bin/console"
#[AsCommand(name: 'app:converter')]
class ConverterCommand extends Command
{
    protected static $defaultName = 'app:converter';
    protected static $defaultDescription = 'Converter valutes.';
    protected $koef = [];

    protected ManagerRegistry $doctrine;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $currencyFrom = $input->getArgument('from');
        $currencyTo = $input->getArgument('to');
        $summa = $input->getArgument('summa');

        if($currencyFrom !== 'EUR') {
            $this->getKoef($currencyFrom, false);
        }

        if($currencyTo !== 'EUR') {
            $this->getKoef($currencyTo, true);
        }

        $result = number_format(($summa * array_product($this->koef)), 10, '.', ' ');

        $output->writeln($summa.' '.$input->getArgument('from').' = '.$result.' '.$input->getArgument('to'));
       
        return Command::SUCCESS;
        

        // or return this if some error happened during the execution
        // (it's equivalent to returning int(1))
        // return Command::FAILURE;

        // or return this to indicate incorrect command usage; e.g. invalid options
        // or missing arguments (it's equivalent to returning int(2))
        // return Command::INVALID
    }

    protected function getKoef(string $currency, bool $trans = false) {
        $criteria = new \Doctrine\Common\Collections\Criteria();
        $criteria
            ->orWhere($criteria->expr()->eq('currencyFrom', $currency))
            ->orWhere($criteria->expr()->eq('currencyTo', $currency));

        $quotes = $this->doctrine->getRepository(Quote::class)->matching($criteria)->toArray();

        if (!$quotes) {
            throw new \LogicException(
                'No found for currency '.$currency
            );
        }

        $flag = false;
        foreach($quotes as $quote){
            if($quote->getCurrencyFrom() === 'EUR'){
                $flag = true;
                $this->koef[] = $this->getRateValue($quote->getRate(), !$trans);
            } elseif($quote->getCurrencyTo() === 'EUR') {
                $flag = true;
                $this->koef[] = $this->getRateValue($quote->getRate(), $trans);
            }
        }

        if (!$flag) {
            foreach($quotes as $quote){
                if($quote->getCurrencyFrom() !== $currency){
                    $this->koef[] = $this->getRateValue($quote->getRate(), !$trans);
                    $next = $quote->getCurrencyFrom();
                } elseif($quote->getCurrencyTo() !== $currency){
                    $this->koef[] = $this->getRateValue($quote->getRate(), $trans);
                    $next = $quote->getCurrencyTo();
                }              
            }
            $this->getKoef($next);
        }
    }

    protected function getRateValue(float $rate, bool $trans = false) {
        if($trans) {
            return 1 / $rate;
        }

        return $rate;
    }

    protected function configure(): void
    {
        $this
            // the command help shown when running the command with the "--help" option
            ->setHelp('This command converter valutes')
            ->addArgument('summa', InputArgument::REQUIRED, 'Summa')
            ->addArgument('from', InputArgument::REQUIRED, 'From')
            ->addArgument('to', InputArgument::REQUIRED, 'To')
        ;
    }
}